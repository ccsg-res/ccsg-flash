/*******************************************************************************
 * FLASH tools for EnviSense and PowerDue boards
 *
 * Copyright (c) 2019 by Bob Iannucci
 *******************************************************************************/

#ifndef __CCSG_FLASH_H__
#define __CCSG_FLASH_H__

#include "variant.h"
#include "CCSG_BoardConfig.h"

uint8_t ccsg_flash_read_byte(uint32_t flashOffset);
void ccsg_flash_write_byte(uint32_t flashOffset, uint8_t data);

#endif  // __CCSG_FILESYSTEM_H__

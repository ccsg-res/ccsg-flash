/*******************************************************************************
 * FLASH tools for EnviSense and PowerDue boards
 *
 * Copyright (c) 2019 by Bob Iannucci
 *******************************************************************************/

#include "CCSG_Flash.h"


#if defined(BOARD_ENVISENSE_FAMILY)
#include "DueFlashStorage.h"

#define WRITE_ENABLE true
DueFlashStorage FLASH;

uint8_t ccsg_flash_read_byte(uint32_t flashOffset)
{
	return FLASH.read(flashOffset);
}

void ccsg_flash_write_byte(uint32_t flashOffset, uint8_t data)
{
	FLASH.write(flashOffset, data);
}

#else

uint8_t ccsg_flash_read_byte(uint32_t flashOffset)
{
	return 0;
}

void ccsg_flash_write_byte(uint32_t flashOffset, uint8_t data) {}

#endif
